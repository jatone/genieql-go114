//+build genieql,generate,scanners

package functions

type Example1Scanner func(e Example1)
type Example2Scanner func(e Example2)
type Example3Scanner func(e Example3)
type Example4Scanner func(e Example4)
