package basic

import (
	"database/sql"
	"math"
	"time"

	"github.com/jackc/pgtype"
)

// DO NOT MODIFY: This File was auto generated by the following command:
// genieql scanner default --output=example_default_scanner.gen.go example example3
// invoked by go generate @ basic/example.go line 8

// ExampleScanner scanner interface.
type ExampleScanner interface {
	Scan(arg0 *example) error
	Next() bool
	Close() error
	Err() error
}

type errExampleScanner struct {
	e error
}

func (t errExampleScanner) Scan(arg0 *example) error {
	return t.e
}

func (t errExampleScanner) Next() bool {
	return false
}

func (t errExampleScanner) Err() error {
	return t.e
}

func (t errExampleScanner) Close() error {
	return nil
}

// ExampleScannerStaticColumns generated by genieql
const ExampleScannerStaticColumns = `"created","email","id","updated"`

// NewExampleScannerStatic creates a scanner that operates on a static
// set of columns that are always returned in the same order.
func NewExampleScannerStatic(rows *sql.Rows, err error) ExampleScanner {
	if err != nil {
		return errExampleScanner{e: err}
	}

	return exampleScannerStatic{
		Rows: rows,
	}
}

// exampleScannerStatic generated by genieql
type exampleScannerStatic struct {
	Rows *sql.Rows
}

// Scan generated by genieql
func (t exampleScannerStatic) Scan(arg0 *example) error {
	var (
		c0 pgtype.Timestamptz
		c1 pgtype.Text
		c2 pgtype.Int8
		c3 pgtype.Timestamptz
	)

	if err := t.Rows.Scan(&c0, &c1, &c2, &c3); err != nil {
		return err
	}

	switch c0.InfinityModifier {
	case pgtype.Infinity:
		tmp := time.Unix(math.MaxInt64, math.MaxInt64)
		arg0.Created = tmp
	case pgtype.NegativeInfinity:
		tmp := time.Unix(math.MinInt64, math.MinInt64)
		arg0.Created = tmp
	default:
		if err := c0.AssignTo(&arg0.Created); err != nil {
			return err
		}
	}

	if err := c1.AssignTo(&arg0.Email); err != nil {
		return err
	}

	if err := c2.AssignTo(&arg0.ID); err != nil {
		return err
	}

	switch c3.InfinityModifier {
	case pgtype.Infinity:
		tmp := time.Unix(math.MaxInt64, math.MaxInt64)
		arg0.Updated = tmp
	case pgtype.NegativeInfinity:
		tmp := time.Unix(math.MinInt64, math.MinInt64)
		arg0.Updated = tmp
	default:
		if err := c3.AssignTo(&arg0.Updated); err != nil {
			return err
		}
	}

	return t.Rows.Err()
}

// Err generated by genieql
func (t exampleScannerStatic) Err() error {
	return t.Rows.Err()
}

// Close generated by genieql
func (t exampleScannerStatic) Close() error {
	if t.Rows == nil {
		return nil
	}
	return t.Rows.Close()
}

// Next generated by genieql
func (t exampleScannerStatic) Next() bool {
	return t.Rows.Next()
}

// NewExampleScannerStaticRow creates a scanner that operates on a static
// set of columns that are always returned in the same order, only scans a single row.
func NewExampleScannerStaticRow(row *sql.Row) ExampleScannerStaticRow {
	return ExampleScannerStaticRow{
		row: row,
	}
}

// ExampleScannerStaticRow generated by genieql
type ExampleScannerStaticRow struct {
	err error
	row *sql.Row
}

// Scan generated by genieql
func (t ExampleScannerStaticRow) Scan(arg0 *example) error {
	var (
		c0 pgtype.Timestamptz
		c1 pgtype.Text
		c2 pgtype.Int8
		c3 pgtype.Timestamptz
	)

	if t.err != nil {
		return t.err
	}

	if err := t.row.Scan(&c0, &c1, &c2, &c3); err != nil {
		return err
	}

	switch c0.InfinityModifier {
	case pgtype.Infinity:
		tmp := time.Unix(math.MaxInt64, math.MaxInt64)
		arg0.Created = tmp
	case pgtype.NegativeInfinity:
		tmp := time.Unix(math.MinInt64, math.MinInt64)
		arg0.Created = tmp
	default:
		if err := c0.AssignTo(&arg0.Created); err != nil {
			return err
		}
	}

	if err := c1.AssignTo(&arg0.Email); err != nil {
		return err
	}

	if err := c2.AssignTo(&arg0.ID); err != nil {
		return err
	}

	switch c3.InfinityModifier {
	case pgtype.Infinity:
		tmp := time.Unix(math.MaxInt64, math.MaxInt64)
		arg0.Updated = tmp
	case pgtype.NegativeInfinity:
		tmp := time.Unix(math.MinInt64, math.MinInt64)
		arg0.Updated = tmp
	default:
		if err := c3.AssignTo(&arg0.Updated); err != nil {
			return err
		}
	}

	return nil
}

// Err set an error to return by scan
func (t ExampleScannerStaticRow) Err(err error) ExampleScannerStaticRow {
	t.err = err
	return t
}
