package drivers_test

import (
	"errors"

	"bitbucket.org/jatone/genieql-go114"
	. "bitbucket.org/jatone/genieql-go114/internal/drivers"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
)

var _ = Describe("pgx", func() {
	It("should register the driver", func() {
		_, err := genieql.LookupDriver(PGX)
		Expect(err).ToNot(HaveOccurred())
	})

	DescribeTable("LookupType",
		lookupDefinitionTest(genieql.MustLookupDriver(PGX).LookupType),
		Entry("example 1 - float32", "float32", "pgtype.Float4", nil),
		Entry("example 2 - *float32", "*float32", "pgtype.Float4", nil),
		Entry("example 3 - float64", "float64", "pgtype.Float8", nil),
		Entry("example 4 - *float64", "*float64", "pgtype.Float8", nil),
		Entry("example 5 - string", "string", "pgtype.Text", nil),
		Entry("example 6 - *string", "*string", "pgtype.Text", nil),
		Entry("example 7 - int16", "int16", "pgtype.Int2", nil),
		Entry("example 8 - *int16", "*int16", "pgtype.Int2", nil),
		Entry("example 9 - int32", "int32", "pgtype.Int4", nil),
		Entry("example 10 - *int32", "*int32", "pgtype.Int4", nil),
		Entry("example 11 - int64", "int64", "pgtype.Int8", nil),
		Entry("example 12 - *int64", "*int64", "pgtype.Int8", nil),
		Entry("example 13 - bool", "bool", "pgtype.Bool", nil),
		Entry("example 14 - *bool", "*bool", "pgtype.Bool", nil),
		Entry("example 15 - time.Time", "time.Time", "pgtype.Timestamptz", nil),
		Entry("example 16 - *time.Time", "*time.Time", "pgtype.Timestamptz", nil),
		Entry("example 17 - unimplemented", "rune", "", errors.New("failed")),
		Entry("example 18 - unimplemented", "*rune", "", errors.New("failed")),
	)
})
