package drivers_test

import (
	"bitbucket.org/jatone/genieql-go114"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestDriver(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Driver Suite")
}

func lookupDefinitionTest(lookup func(string) (genieql.ColumnDefinition, error)) func(typs, exprs string, err error) {
	return func(typename, exprs string, err error) {
		result, failure := lookup(typename)

		if err != nil {
			Expect(failure).To(HaveOccurred())
		} else {
			Expect(failure).To(Succeed())
		}

		Expect(result.ColumnType).To(Equal(exprs))
	}
}
