package main

import (
	"fmt"
	"go/ast"
	"go/build"
	"go/parser"
	"log"
	"os"
	"path/filepath"

	"bitbucket.org/jatone/genieql-go114"
	"bitbucket.org/jatone/genieql-go114/cmd"
	"bitbucket.org/jatone/genieql-go114/generators"
	"github.com/alecthomas/kingpin"
)

// GenerateScanner root command for generating scanners.
type GenerateScanner struct {
	buildInfo
}

func (t *GenerateScanner) configure(cmd *kingpin.CmdClause) *kingpin.CmdClause {
	scanner := cmd.Command("scanners", "commands for generating scanners")
	(&generateScannerCLI{
		buildInfo: t.buildInfo,
	}).configure(scanner).Default()
	(&generateScannerTypes{
		buildInfo: t.buildInfo,
	}).configure(scanner)

	return scanner
}

type generateScannerCLI struct {
	buildInfo
	scanner    string
	configName string
	output     string
	pkg        string
}

func (t *generateScannerCLI) configure(cmd *kingpin.CmdClause) *kingpin.CmdClause {
	cli := cmd.Command("cli", "generates a scanner from the provided expression").Action(t.execute)
	cli.Flag("config", "name of the genieql configuration to use").Default(defaultConfigurationName).StringVar(&t.configName)
	cli.Flag("scanner", "definition of the scanner, must be a valid go expression").StringVar(&t.scanner)
	cli.Flag("output", "output filename").Short('o').StringVar(&t.output)
	cli.Flag("package", "package to search for constant definitions").
		Default(t.CurrentPackageImport()).StringVar(&t.pkg)

	return cli
}

func (t *generateScannerCLI) execute(*kingpin.ParseContext) (err error) {
	var (
		ctx  generators.Context
		node *ast.File
	)

	if ctx, err = loadGeneratorContext(build.Default, t.configName, t.pkg); err != nil {
		return err
	}

	if node, err = parser.ParseFile(ctx.FileSet, "example", fmt.Sprintf("package foo; %s", t.scanner), 0); err != nil {
		return err
	}

	g := genieql.MultiGenerate(mapDeclsToGenerator(func(d *ast.GenDecl) []genieql.Generator {
		return generators.ScannerFromGenDecl(
			d,
			generators.ScannerOptionContext(ctx),
		)
	}, genieql.SelectFuncType(genieql.FindTypes(node)...)...)...)

	hg := headerGenerator{
		fset: ctx.FileSet,
		pkg:  ctx.CurrentPackage,
		args: os.Args[1:],
	}

	pg := printGenerator{
		pkg:      ctx.CurrentPackage,
		delegate: genieql.MultiGenerate(hg, g),
	}

	if err = cmd.WriteStdoutOrFile(pg, t.output, cmd.DefaultWriteFlags); err != nil {
		log.Fatalln(err)
	}

	return nil
}

type generateScannerTypes struct {
	buildInfo
	configName string
	output     string
	pkg        string
}

func (t *generateScannerTypes) configure(cmd *kingpin.CmdClause) *kingpin.CmdClause {
	c := cmd.Command("types", "generates a scanner from the provided expression").Action(t.execute)
	c.Flag("config", "name of the genieql configuration to use").Default(defaultConfigurationName).StringVar(&t.configName)
	c.Flag("output", "output filename").Short('o').StringVar(&t.output)
	c.Flag("package", "package to search for constant definitions").
		Default(t.CurrentPackageImport()).StringVar(&t.pkg)

	return c
}

func (t *generateScannerTypes) execute(*kingpin.ParseContext) (err error) {
	var (
		ctx  generators.Context
		tags = []string{
			"genieql", "generate", "scanners",
		}
	)

	if ctx, err = loadGeneratorContext(build.Default, t.configName, t.pkg, tags...); err != nil {
		return err
	}

	taggedFiles, err := findTaggedFiles(t.pkg, tags...)
	if err != nil {
		return err
	}

	if len(taggedFiles.files) == 0 {
		log.Println("no files tagged")
		// nothing to do.
		return nil
	}

	g := []genieql.Generator{}
	genieql.NewUtils(ctx.FileSet).WalkFiles(func(path string, file *ast.File) {
		if !taggedFiles.IsTagged(filepath.Base(path)) {
			return
		}

		scanners := mapDeclsToGenerator(func(d *ast.GenDecl) []genieql.Generator {
			return generators.ScannerFromGenDecl(
				d,
				generators.ScannerOptionContext(ctx),
			)
		}, genieql.SelectFuncType(genieql.FindTypes(file)...)...)

		g = append(g, scanners...)
	}, ctx.CurrentPackage)

	mg := genieql.MultiGenerate(g...)

	hg := headerGenerator{
		fset: ctx.FileSet,
		pkg:  ctx.CurrentPackage,
		args: os.Args[1:],
	}

	pg := printGenerator{
		pkg:      ctx.CurrentPackage,
		delegate: genieql.MultiGenerate(hg, mg),
	}

	if err = cmd.WriteStdoutOrFile(pg, t.output, cmd.DefaultWriteFlags); err != nil {
		log.Fatalln(err)
	}

	return nil
}
