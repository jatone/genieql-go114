package compiler

import (
	"bitbucket.org/jatone/genieql-go114/internal/x/errorsx"
)

// Well known errors for the compiler
const (
	ErrNoMatch = errorsx.String("no match")
)
